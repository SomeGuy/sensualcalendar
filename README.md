# SensualCalendar

A free calendar to help encourage marital intimacy.

This is a FREE resource for PERSONAL use. Adapt it and modify it however you want, but you can not sell it. All of the images belong to https://www.christianfriendlysexpositions.com/ but the author did all the work in creating the template, calendar, ect. PLEASE go to https://cfsps.co and support them for all the resources, images, descriptions, and items they make available for blessing marital intimacy.

There are two versions of the calendar in two different formats. The first format is an editable fodt file. It should be editable without issue in any version of LibreOffice (https://www.libreoffice.org/) which is a free office suite though the author used version 6.1. The second format is pdf for those that just want to print off a copy for PERSONAL USE.

The two different versions are just celebration days. The 2021 is standard US holidays. The 2021_SensualDate version has nearly all of the dates listed from the https://gitlab.com/SomeGuy/SensualDate repository. It is recommended that you look at the SensualDate items first. Many can be used as playful ways of engaging and spicing up things in your marriage. However, there may be dates/activities you don't want to participate in.

The calendar is broken out into weeks where there is a position challenge every week. In addition to the position, there is an extra set of fun playful challenges that couples may also participate in. Do as many or as little of the challenges as you deem appropriate. Or come up with your own. Just have fun with your marital intimacy!